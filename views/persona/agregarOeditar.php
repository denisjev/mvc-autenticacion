<?php
    include_once("menu.php");
?>

<form method="POST">
        <input type="hidden" name="module" value="persona" />
        <input type="hidden" name="action" value="<?php echo $eventoFormulario; ?>" />
        <p>Id: <input type="number" name="id" value="<?php echo $personaActual->id; ?>"
            <?php if($eventoFormulario == "actualizarPersona") echo "readonly"; ?> /></p>
            
        <p>Nombre: <input type="text" name="nombre" value="<?php echo $personaActual->nombre; ?>" /></p>
        <p>Edad: <input type="number" name="edad" value="<?php echo $personaActual->edad; ?>" /></p>
        <p>Sexo: <select name="sexo">
                    <option value="Masculino" <?php if($personaActual->sexo == "Masculino") echo "selected"; ?>>
                        Masculino
                    </option>
                    <option value="Femenino" <?php if($personaActual->sexo == "Femenino") echo "selected"; ?>>
                        Femenino
                    </option>
                </select></p>
        <p><input type="submit" name="<?php echo $eventoFormulario; ?>" value="Guardar" /></p>
</form>